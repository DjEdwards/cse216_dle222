1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="edu.lehigh.cse216.dle222"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="15"
8-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml
9        android:targetSdkVersion="29" />
9-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.INTERNET" />
11-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:5:5-67
11-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:5:22-64
12
13    <application
13-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:7:5-29:19
14        android:allowBackup="true"
14-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:8:9-35
15        android:appComponentFactory="androidx.core.app.CoreComponentFactory"
15-->[androidx.core:core:1.1.0] /Users/DJ/.gradle/caches/transforms-2/files-2.1/e9dab9c67c1ee3668e0bbf86c6d8cbd8/core-1.1.0/AndroidManifest.xml:24:18-86
16        android:debuggable="true"
17        android:icon="@mipmap/ic_launcher"
17-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:9:9-43
18        android:label="@string/app_name"
18-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:10:9-41
19        android:roundIcon="@mipmap/ic_launcher_round"
19-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:11:9-54
20        android:supportsRtl="true"
20-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:12:9-35
21        android:testOnly="true"
22        android:theme="@style/AppTheme"
22-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:13:9-40
23        android:usesCleartextTraffic="true" >
23-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:14:9-44
24        <activity
24-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:15:9-18:68
25            android:name="edu.lehigh.cse216.dle222.SecondActivity"
25-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:16:13-43
26            android:label="@string/title_activity_second"
26-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:17:13-58
27            android:theme="@style/AppTheme.NoActionBar" />
27-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:18:13-56
28        <activity
28-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:19:9-28:20
29            android:name="edu.lehigh.cse216.dle222.MainActivity"
29-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:20:13-41
30            android:label="@string/app_name"
30-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:21:13-45
31            android:theme="@style/AppTheme.NoActionBar" >
31-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:22:13-56
32            <intent-filter>
32-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:23:13-27:29
33                <action android:name="android.intent.action.MAIN" />
33-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:24:17-69
33-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:24:25-66
34
35                <category android:name="android.intent.category.LAUNCHER" />
35-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:26:17-77
35-->/Users/DJ/Desktop/cse216_dle222/cse216_dle222/android/app/src/main/AndroidManifest.xml:26:27-74
36            </intent-filter>
37        </activity>
38    </application>
39
40</manifest>
